package messagetypes;

import java.io.Serializable;

public class ShutDown implements Serializable {

    private String identifier;

    public ShutDown(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() { return identifier; }
}
