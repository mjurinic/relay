package messagetypes;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Public and private IP address and port number of the source
 * that is currently broadcasting
 */
public class OnlineStreamInfo extends StreamRegister implements Serializable {

    private long timestamp;
    private StreamAdvertisement localSourceInfo;

    public OnlineStreamInfo(InetAddress ip, int port, StreamAdvertisement localSourceInfo) {
        super(ip, port);

        this.localSourceInfo = localSourceInfo;
        timestamp = System.currentTimeMillis() / 1000L;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public InetAddress getPrivateIp() { return localSourceInfo.getIp(); }

    public int getPrivatePort() { return localSourceInfo.getPort(); }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
