package messagetypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RelayList implements Serializable {

    private int listLen;
    private List<SocketIdentifier> relayServers;

    public RelayList() {
        relayServers = new ArrayList<SocketIdentifier>();
    }

    public void addRelayServer(SocketIdentifier relayItem) {
        this.relayServers.add(relayItem);
    }

    public void setRelayServers(List<SocketIdentifier> relayServers) {
        this.relayServers = relayServers;
    }

    public List<SocketIdentifier> getRelayServers() {
        return relayServers;
    }
}
