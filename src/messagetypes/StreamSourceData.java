package messagetypes;

import java.io.Serializable;

/**
 * Information filled by the server
 * Server sends a request message in order to get
 * both private and public IP address and port number of the source
 * Also contains the public IP address and port number of the player
 *
 *  - Not really sure why we need to store all this information
 *    Wouldn't it be enough to just send the public address and port number
 *    of the source?
 *    We already know the public IP address of the player since
 *    the packet request message got from there and the player itself has no use of it?
 *    Also not sure what the deal is with the private address of the source...maybe it
 *    will be used in "behind NAT" situations?
 */
public class StreamSourceData implements Serializable {

    private String identifier;
    private SocketIdentifier playerIdentifier;
    private OnlineStreamInfo sourceIdentifier;

    public StreamSourceData(SocketIdentifier playerIdentifier, OnlineStreamInfo sourceIdentifier) {
        this.playerIdentifier = playerIdentifier;
        this.sourceIdentifier = sourceIdentifier;
    }

    public String getIdentifier() { return identifier; }

    public SocketIdentifier getPlayerIdentifier() {
        return playerIdentifier;
    }

    public OnlineStreamInfo getSourceIdentifier() {
        return sourceIdentifier;
    }

    public void setIdentifier(String identifier) { this.identifier = identifier; }
}
