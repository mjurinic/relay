package messagetypes;

import java.io.Serializable;

public class RelayListRequest implements Serializable {

    private byte type;

    public RelayListRequest() {
        this.type = MessageType.MSG_REQ_RELAY_LIST.getValue();
    }

    public byte getType() {
        return type;
    }
}
