package messagetypes;


import java.io.Serializable;

public class SourceReady implements Serializable {

    private String identifier;

    public SourceReady(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() { return identifier; }
}
