package messagetypes;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Contains local IP address and port number of the source
 */
public class StreamAdvertisement extends SocketIdentifier implements Serializable {

    public StreamAdvertisement(String identifier, InetAddress ip, int port) {
        super(identifier, ip, port);
    }
}
