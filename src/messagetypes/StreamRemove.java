package messagetypes;

import java.io.Serializable;

/**
 * Created by root on 21.07.15..
 */
public class StreamRemove implements Serializable {

    private String identifier;

    public StreamRemove(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }
}
