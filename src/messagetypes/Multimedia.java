package messagetypes;

import java.io.Serializable;

/**
 * stream ID, packet num., timestamp, frame data
 */

public class Multimedia implements Serializable {

    private String identifier;
    private byte[] frameData;

    public Multimedia(String identifier, byte[] frameData) {
        this.identifier = identifier;
        this.frameData = frameData;
    }

    public String getIdentifier() { return identifier; }

    public byte[] getFrameData() { return frameData; }
}
