package com.mjurinic;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class Config {

    private int localPort;
    private boolean stateMaintain;

    public Config() {
        parseJson(readFromFile());
    }

    public int getLocalPort() {
        return localPort;
    }

    public boolean getStateMaintain() { return stateMaintain; }

    private String readFromFile() {
        BufferedReader br = null;
        String ret = null;

        try {
            URL url = getClass().getResource("server.ini"); 
                       
            FileReader fr = new FileReader(url.getPath());
            br = new BufferedReader(fr);

            StringBuilder sb = new StringBuilder();
			String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }

            ret = sb.toString();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
            	if (br != null)
                	br.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ret;
    }

    private void parseJson(String jsonString) {
        try {
            JSONObject obj = new JSONObject(jsonString);

            localPort = obj.getInt("local_port");
            stateMaintain = obj.getBoolean("state_maintain");

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
