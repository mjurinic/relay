package com.mjurinic;

import MyUdpServer.ReceivedPacket;
import MyUdpServer.SerializedObject;
import MyUdpServer.UdpServer;
import messagetypes.Multimedia;
import messagetypes.Ping;
import messagetypes.PleaseForward;
import messagetypes.Pong;

import java.net.InetAddress;
import java.util.Date;

/**
 * Currently does NOT support state maintain!
 */

public class Main {

    private static Config serverConfig;
    private static UdpServer mServer;
    private static InetAddress sendersIP;
    private static int sendersPort;

    private static InetAddress destinationIP;
    private static int destinationPort;
    private static Object data;

    public static void main(String[] args) {
        init();
        handleRequests();
    }

    private static void handleRequests() {
        System.out.println("LISTENING FOR REQUESTS");
        System.out.println("----------------------");

        while (true) {
            ReceivedPacket receivedPacket = mServer.receivePacket();
            String timestamp = new java.text.SimpleDateFormat("h:mm:ss a").format(new Date());

            try {
                Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                sendersIP = receivedPacket.getIp();
                sendersPort = receivedPacket.getPort();

                System.out.println("["+ timestamp +"] Request received from: " + sendersIP + ":" + sendersPort);

                /**
                 * MSG_PING
                 */
                if (receivedObject instanceof Ping) {
                    System.out.println("["+ timestamp +"] Message type: MSG_PING");
                    mServer.sendPacket(sendersIP, sendersPort, SerializedObject.Serialize(new Pong()));
                }

                /**
                 * MSG_PLEASE_FORWARD
                 *
                 * Should check if PARAM TYPE MESSAGE HAS BEEN SENT - forward it to another port
                 */
                else if (receivedObject instanceof PleaseForward) {
                    System.out.println("["+ timestamp +"] Message type: MSG_PLEASE_FORWARD");

                    PleaseForward pleaseForward = (PleaseForward) receivedObject;

                    String destinationIP = pleaseForward.getIp().replace("/", "");
                    int destinationPort = pleaseForward.getPort();
                    Object data = pleaseForward.getObject();

                    mServer.sendPacket(InetAddress.getByName(destinationIP), destinationPort, SerializedObject.Serialize(data));
                }

                else {
                    System.out.println("["+ timestamp +"] UNKNOWN REQUEST!");
                }
            }
            catch (NullPointerException e) {
                //
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void init() {
        System.out.println("\n\n----------------------------");
        System.out.println("STARTING INITIALIZATION FAZE");
        System.out.println("----------------------------\n");

        serverConfig = new Config();
        mServer = new UdpServer(serverConfig.getLocalPort());

        System.out.println("Listening on port: " + serverConfig.getLocalPort());
        System.out.println("Maintain state: " + serverConfig.getStateMaintain());

        System.out.println("\n----------------------------");
        System.out.println("INITIALIZATION FAZE FINISHED");
        System.out.println("----------------------------\n");
    }
}
